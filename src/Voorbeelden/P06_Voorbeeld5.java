package Voorbeelden;

/* 
 Schrijf een programma om willekeurig 100 getallen te genereren die liggen tussen a en b.
 Deze waarden dienen als gegevens via het toetsenbord ingegeven te worden.
 Hiervoor gebruik je de volgende formule: a+(b-a) * random(). 
 */

public class P06_Voorbeeld5 {

	public static void main(String[] args) {
		int a, b;
		a = Invoer.leesInt("Minimum: ");
		b = Invoer.leesInt("Maximum: ");
		doeRandom(a, b);

	}

	private static void doeRandom(int a, int b) {
		int willekeurig;
		for (int i = 1; i <= 100; i++) {
			willekeurig = (int) (a + (b - a) * Math.random());
			System.out.println(willekeurig);
		}
	}

}
