package Voorbeelden;
/*
Schrijf een programma om de diameter van een cirkel te berekenen als de oppervlakte van de cirkel gegeven is.
Formule oppervlakte: PI * r**2
Het resultaat dient afgerond te worden op 2 cijfers na de komma.
*/
public class P06_Voorbeeld4 {

	public static void main(String[] args) {
		double oppVl;
		oppVl = Invoer.leesInt("Geef de oppervlakte in: ");
		berekenDiameter(oppVl);

	}
	
	public static void berekenDiameter (double oppVl) {
		double diameter;
		diameter = Math.round((Math.sqrt(oppVl / Math.PI) * 2) * 100.0) / 100.0;
		System.out.println(diameter);
	}

}