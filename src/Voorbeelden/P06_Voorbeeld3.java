package Voorbeelden;
/* 
Schrijf een programma om x**3 te berekenen. Hierbij is x geheel en het resultaat moet ook geheel zijn. x moet via het toetsenbord ingegeven worden. 
 */
public class P06_Voorbeeld3 {

	public static void main(String[] args) {
		int x;
		x = Invoer.leesInt("Geef een geheel getal in: ");
		berekenMacht(x);
	}
	
	public static void berekenMacht (int getal) {
		getal = (int) Math.pow(getal, 3.0);
		System.out.println(getal);
		
	}
}
