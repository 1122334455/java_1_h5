package Voorbeelden;
/*
Schrijf een programma om 2 tekstvariabelen naam en voornaam samen te voegen tot 1 variabele.
De nieuwe variabele bestaat uit de eerste letter van de voornaam in hoofdletters gevolgd door de achternaam.
De 1ste letter van de achternaam moet ook een hoofdletter zijn, de andere letters zijn kleine letters.
 */
public class P10_Voorbeeld5 {

	public static void main(String[] args) {
		String voornaam, achternaam, naam;
		voornaam = Invoer.leesString("Geef de voornaam in: ");
		achternaam = Invoer.leesString("Geef de achternaam in: ");
		naam = voornaam.substring(0, 1).toUpperCase() + ". " + achternaam.substring(0, 1).toUpperCase() + achternaam.substring(1).toLowerCase() + "";;
		System.out.println(naam);
	}
}