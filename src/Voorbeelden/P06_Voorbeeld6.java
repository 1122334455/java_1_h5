package Voorbeelden;
// Schrijf een programma om willekeurig 10 gehele getallen (>=0 en <15) te genereren.

public class P06_Voorbeeld6 {

	public static void main(String[] args) {
		int willekeurig;
		for (int i = 1; i <= 10; i++) {
			willekeurig = (int) (Math.random() * 16); // 0 + (16 - 0) -> * 16
			System.out.print(willekeurig + "\t");
		}
	}

}
// 0 - 15 (incl) met decimale getallen -> if gebruiken en afkappen boven de 15.