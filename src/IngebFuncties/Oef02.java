/*	
 * Schrijf een programma om PI * sqrt(xˆ3 + yˆ4) te berekenen. x is een ingevoerd geheel getal, y is een random geheel getal (>-10 en <10).
 */
package IngebFuncties;

import Functies.Invoer;

public class Oef02 {

	public static void main(String[] args) {
		int x, y;
		double res;
		x = Invoer.leesInt("Geef een geheel getal in: ");
		y = (int) (-9 + (10-(-9) * Math.random()));
		if (Math.pow(x, 3) + Math.pow(y, 4) >= 0) {
			res = Math.PI * (Math.sqrt(Math.pow(x,3) + Math.pow(y,4)));
			System.out.println("Het resultaat is: "+ res);
		} else {
			System.out.println("Deze berekening kan niet voltooid worden!");
		}
	}

}
