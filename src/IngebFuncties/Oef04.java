/*
 * Bepaal in een tekst de positie van de eerste e of E en vervang vanaf dan alle volgende letters door hoofdletters.
 */
package IngebFuncties;

import Voorbeelden.Invoer;

public class Oef04 {

	public static void main(String[] args) {
		String tekst, klein;
		int positie;
		tekst = Invoer.leesString("Geef een tekst in: ");
		klein = tekst.toLowerCase();
		positie = klein.indexOf('e');
		System.out.println(tekst.substring(0, positie)
				+ tekst.substring(positie).toUpperCase());
	}

}
