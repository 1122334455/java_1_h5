/*
 * Als de lengte van een tekst deelbaar is door 3 -> hoofdletters
 * niet deelbaar door 3 -> kleine letters 
 */
package IngebFuncties;

import Voorbeelden.Invoer;

public class ExtraOef02 {

	public static void main(String[] args) {
		String tekst;
		tekst = Invoer.leesString("Geef een tekst in: ");
		int lengte = tekst.length();
		if (lengte % 3 == 0) {
			System.out.println(tekst.toUpperCase());
		} else {
			System.out.println(tekst.toLowerCase());
		}

	}

}
