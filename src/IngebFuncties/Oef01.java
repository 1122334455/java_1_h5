/*Schrijf een programma om 1ˆ2 + 3ˆ3 + 5ˆ4...41ˆ22 te berekenen, 41 wordt ingegeven.
 * Doe een foutencontrole op het getal, dit moet oneven zijn.
 */

package IngebFuncties;

import Functies.Invoer;

public class Oef01 {

	public static void main(String[] args) {
		int getal;
		int exp = 2;
		double som = 0;
		getal = Invoer.leesInt("Geef een getal in: ");
		while (getal % 2 == 0) {
			System.out.println("Foutieve ingave! Geef een oneven getal in!");
			getal = Invoer.leesInt("Geef een getal in: ");
		}
		
		for (int i = 1; i <= getal; i+=2) {
			som = som + Math.pow(getal, exp);
			exp++;
		}
		System.out.println("Het resultaat is: "+ som);
	}

}
