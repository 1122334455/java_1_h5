/*
 * Schrijf een programma om de middelste of twee middelste letters van een woord om te wisselen.
 */
package IngebFuncties;

import Functies.Invoer;

public class ExtraOef04 {

	public static void main(String[] args) {
		String woord;
		char kar1, kar2;
		woord = Invoer.leesString("Geef een woord in: ");
		if (woord.length() % 2 == 0) {
			kar1 = woord.charAt((woord.length() / 2) - 1);
			kar2 = woord.charAt(woord.length() / 2);
			System.out.println(woord.substring(0, (woord.length() / 2) - 1) + kar2 + kar1 + woord.substring((woord.length() / 2) + 1));
		} else {
			kar1 = woord.charAt(woord.length() / 2);
			kar2 = woord.charAt((woord.length() / 2) + 1);
			System.out.println(woord.substring(0, (woord.length() / 2)) + kar2 + kar1 + woord.substring((woord.length() / 2) + 1));
		}

	}
}
