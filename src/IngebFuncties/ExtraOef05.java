/*
 * Een code bestaat uit 4 letters, gevolgd door 4 cijfers. Schrijf een programma om de code wijzigen in 4 cijfers, gevolgd door 4 letters.
 */
package IngebFuncties;

import Functies.Invoer;

public class ExtraOef05 {

	public static void main(String[] args) {
		String code, letters, cijfers;
		code = Invoer.leesString("Geef een code in (4 letters, 4 cijfers): ");
		letters = code.substring(0, 4);
		cijfers = code.substring(4);
		System.out.println(cijfers + letters);

	}

}
