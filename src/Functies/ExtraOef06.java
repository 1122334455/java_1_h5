/*
 * Schrijf een programma om van 2 tekstvariabelen, 1 tekstvariabele te maken. De nieuwe variabele bekom je door de eerste 4 letters in hoofdletters te nemen (<4 -> * in de plaats invullen).
 * Nadien volgende de laatste 4 letters (<4 -> + vooraan aanvullen)
 */
package Functies;

public class ExtraOef06 {

	public static void main(String[] args) {
		String tekst1, tekst2, sterren = "", plussen = "", nieuweTekst1, nieuweTekst2;
		tekst1 = Invoer.leesString("Geef de eerste tekst in: ");
		tekst2 = Invoer.leesString("Geef de tweede tekst in: ");
		if (tekst1.length() < 4) {
			for (int i = 1; i<= 4 - tekst1.length();i++) {
				sterren += "*";
			}
			nieuweTekst1 = tekst1 + sterren;
		} else {
			nieuweTekst1 = tekst1.substring(0, 4);
		}
		if (tekst2.length() < 4) {
			for (int i = 1; i<= 4 - tekst2.length();i++) {
				plussen += "+";
			}
			nieuweTekst2 = plussen + tekst2;
		} else {
			nieuweTekst2 = tekst2.substring(0, 4);
		}
		System.out.println(nieuweTekst1 + nieuweTekst2);
	}

}
