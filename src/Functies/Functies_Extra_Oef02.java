/*
 * Bereken het kindergeld van een gezin. Het aantal kinderen wordt ingegeven. Per kind wordt de leeftijd en een code ingegeven, deze code bevat H als het kind gehandicapt is.
 * De bijslag wordt zo berekend:
 * Basisbedrag = 75 euro voor het eerste kind, voor elk volgende kind wordt dit verhoogd met 70 euro.
 * Voor een gehandicapt kind is dit basisbedrag 300 euro.
 * Na de 6de en 12de verjaardag wordt dit verhoogd met 25 euro.
 * Gebruik een functie voor de berekening, de totale bijslag wordt afgedrukt.
 */
package Functies;
public class Functies_Extra_Oef02 {
	public static void main(String[] args) {
		int aantKind, leeftijd, totBijslag = 0;
		char code;
		aantKind = Invoer.leesInt("Geef het aantal kinderen in: ");
		for (int teller = 1; teller <= aantKind; teller++) {
			leeftijd = Invoer.leesInt("Geef de leeftijd in: ");
			code = Invoer.leesChar("Geef de code in: ");
			totBijslag += berekenBijslag(leeftijd, code, teller);
		}
		System.out.println("Totale bijslag: " + totBijslag);
	}

	private static int berekenBijslag(int leeftijd, char code, int teller) {
		int bedrag = 0;
		if (code == 'H') {
			bedrag += 300;
		} else if (teller == 1) {
			bedrag += 75;
		} else {
			bedrag += 70;
		}
		if (leeftijd == 6 || leeftijd == 12) {
			bedrag += 25;
		}
		return bedrag;
	}
}
