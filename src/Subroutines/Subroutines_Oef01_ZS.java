package Subroutines;
public class Subroutines_Oef01_ZS {

	public static void main(String[] args) {
		int lengte, hoogte;
		char teken;
		lengte = Invoer.leesInt("Geef de lengte in: ");
		hoogte = Invoer.leesInt("Geef de hoogte in: ");
		teken = Invoer.leesChar("Geef het teken in: ");
		for (int i = 1; i <= lengte; i++) {
			System.out.print(teken);
		}
		System.out.println();

		for (int i = 1; i <= hoogte; i++) {
			System.out.print(teken);
			for (int j = 1; j <= lengte - 2; j++) {
				System.out.print(" ");
			}
			System.out.println(teken);
		}
		for (int i = 1; i <= lengte; i++) {
			System.out.print(teken);
		}
	}
}
