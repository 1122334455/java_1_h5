package Subroutines;

public class Subroutines_Oef03 {
	static double uren;
	static int aantPers1, aantPers2, totPers;

	public static void main(String[] args) {
		int vierkanteM;
		double kostPrijs = 0;
		vierkanteM = Invoer.leesInt("Geef het aantal vierkante meter in: ");
		while (vierkanteM != 0) {
			kostPrijs = berekenprijs(vierkanteM);
			berekenAantPers(kostPrijs);
			double tijdPers2 = uren % 8;
			System.out.println("Totale prijs: " + kostPrijs + " euro");
			System.out.println("Totaal aantal personen: " + totPers);
			System.out.println(aantPers1 + " personen werken 8u");
			if (aantPers2 != 0) {
				System.out.println(aantPers2 + " persoon werkt " + tijdPers2
						+ "u");
			}
			vierkanteM = Invoer
					.leesInt("\nGeef het aantal vierkante meter in: ");
		}

	}

	private static double berekenprijs(int vierkanteM) {
		uren = vierkanteM / 160.0;
		return uren * 12.5;
	}

	private static void berekenAantPers(double kostPrijs) {
		totPers = (int) (Math.ceil(uren / 8));
		aantPers1 = (int) (uren / 8);
		aantPers2 = totPers - aantPers1;
	}
}