package Subroutines;
public class Subroutines_Oef02_ZS {

	public static void main(String[] args) {
		int tellerSpatie, tellerTeken = 1, breedte;
		char teken;
		breedte = Invoer.leesInt("Geef de breedte in: ");
		tellerSpatie = breedte - 1;
		teken = Invoer.leesChar("Geef het teken in: ");
		for (int i = 1; i <= breedte; i++) {
			for (int j = 1; j <= tellerSpatie; j++) {
				System.out.print(" ");
			}
			for (int k = 1; k <= tellerTeken; k++) {
				System.out.print(teken);
			}
			tellerSpatie -= 1;
			tellerTeken += 1;
			System.out.println();
		}

	}
}
