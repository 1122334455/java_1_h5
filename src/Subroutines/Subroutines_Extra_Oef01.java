package Subroutines;

public class Subroutines_Extra_Oef01 {

	public static void main(String[] args) {
		int grootte = 0;
		char teken = 0;
		grootte = Invoer.leesInt("Geef de grootte van de driehoek in: ");
		teken = Invoer.leesChar("Geef het teken in: ");
		System.out.println();
		tekenDriehoek(grootte, teken);

	}

	private static void tekenDriehoek(int grootte, char teken) {
		for (int i = 1; i <= grootte; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(teken);
			}
			System.out.println();
			teken += 1;
		}
		
	}

}
