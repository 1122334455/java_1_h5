package Subroutines;

import java.io.*;
import java.util.Calendar;

public class Subroutines_Extra_Oef03 {
	static int prio = 0;

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer("H5_oefening3_extra.txt");
		int aanVrN = 0, iM, iV, aK;
		String code, nM, nV;
		char gBouwL;
		code = a.leesRegel();
		while (!code.substring(0, 1).equals("S")) {
			nM = a.leesRegel();
			nV = a.leesRegel();
			iM = a.leesInt();
			iV = a.leesInt();
			aK = a.leesInt();
			aanVrN += 1;
			berekenPrio(code, iM, iV, aK);
			gBouwL = bepaalBouwL(code, nM, nV, iM, iV, aK);
			drukAf(aanVrN, nM, nV, prio, gBouwL);
			code = a.leesRegel();
		}

	}

	private static void berekenPrio(String code, int iM, int iV, int aK) {
		int codeJaar = 0, gInkomen, huidigJaar;
		char codeTeken;
		gInkomen = iM + iV;
		huidigJaar = Calendar.getInstance().get(Calendar.YEAR);
		codeTeken = code.charAt(0);
		if (code.length() > 1) {
			codeJaar = Integer.parseInt(code.substring(1, 4));
			if (huidigJaar - codeJaar < 5 && codeTeken == 'J') {
				prio = 5;
			} else if (gInkomen < 2000) {
				if (aK >= 3) {
					prio = 1;
				} else {
					prio = 2;
				}
			} else if (gInkomen < 2500) {
				prio = 3;
			} else {
				prio = 4;
			}
		}
	}

	private static char bepaalBouwL(String code, String nM, String nV, int iM, int iV, int aK) {
		int ginkomen = iM + iV;
		char bouwL = ' ';
		char codeTeken;
		codeTeken = code.charAt(0);
		if (codeTeken == 'N' && ((nM.equals("xx") && iM == 0) || (nV.equals("xx") && iV == 0))) {
			if (ginkomen < 1500 || aK >= 1) {
				bouwL = 'J';
			}
		}
		return bouwL;
	}

	private static void drukAf(int aanVrN, String nM, String nV, int prio,
			char gBouwL) {
		String famNaam, sterren = "";
		nM = nM.replaceAll("  ", " ");
		nM = nM.replaceAll("  ", " ");
		String[] naamMan = (nM.split(" "));
		String[] naamVrouw = nV.split(" ");
		if (nM.equals("xx")) {
			famNaam = "Mevrouw " + naamVrouw[1].charAt(0) + ". " + naamVrouw[0];
		} else if (nV.equals("xx")) {
			famNaam = "De Heer " + naamMan[1].charAt(0) + ". " + naamMan[0];
		} else {
			famNaam = "De Heer en Mevrouw " + naamMan[1].charAt(0) + ". "
					+ naamMan[0] + "-" + naamVrouw[0];
		}
		System.out.format("%-5d", aanVrN);
		System.out.format("%-55s", famNaam);
		for (int i = 1; i <= prio; i++) {
			sterren += "*";
		}
		System.out.format("%-10s%-10s", sterren, gBouwL);
		System.out.println();
	}
}